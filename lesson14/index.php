<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Таблица");

use Bitrix\Main\Application;
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Context;
use Bitrix\Main\SystemException;

$server = Context::getCurrent()->getServer();
$request = Application::getInstance()->getContext()->getRequest();


function view_table($record)
{
    echo '<table style="margin:40px;">';
    echo '<tr>';
    echo '<th>Ид Инфоблока</th>';
    echo '<th>Текст</th>';
    echo '<th>Подробное описание</th>';
    echo '</tr>';
    foreach ($record as $row) {
        echo '<tr>';
        foreach ($row as $key => $value) {
            echo '<td>';
            echo $row[$key];
            echo '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
}

if ($server->getRequestMethod() == "GET" and !empty($request->getQuery('iblock'))) {
    try {
        //Получение индфикатора инфоблока
        $iblock = $request->getQuery('iblock');
        // Получение записей таблицы 
        $record = Application::getConnection()
            ->query("select IBLOCK_ID, PREVIEW_TEXT, DETAIL_TEXT from b_iblock_element where IBLOCK_ID = " . $request->getQuery('iblock'))
            ->fetchAll();

        if (count($record) > 0) {
            // Вывод таблицы
            view_table($record);
        } else {
            echo "Ничего не найдено";
            // Логирование 
        }
    } catch (SystemException $exception) {
        echo $exception->getMessage();
    }

    if (count($record) == 0)
    {
        Debug::writeToFile(
            $_GET,
            'Ошибка не найден iblock ' . date('d.m.Y H:i:s') . ' | ' .  __FILE__,
            'local/logs/log_something.txt'
        );
    }
}

if (empty($request->getQuery('iblock'))) {
    Debug::writeToFile(
        $_GET,
        'Ошибка не передано значение элемента iblock ' . date('d.m.Y H:i:s') . ' | ' .  __FILE__,
        'local/logs/log_something.txt'
    );
}
