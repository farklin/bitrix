<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Услуги");
?><?$APPLICATION->IncludeComponent(
	"bitrix:news",
	"templates.php",
	Array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array("ID","CODE","NAME","DETAIL_TEXT","DETAIL_PICTURE",""),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Услуги",
		"DETAIL_PROPERTY_CODE" => array("TYPES",""),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "SERVISES",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array("NAME","DETAIL_TEXT",""),
		"LIST_PROPERTY_CODE" => array("TYPES",""),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_FOLDER" => "/services/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => Array("detail"=>"#ELEMENT_CODE#/","news"=>"","section"=>""),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N"
	)
);?><br>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"slider-partners",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("NAME","SORT","PREVIEW_PICTURE",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "partners",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("LINK","SRC",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
<div class="page-section services" id="services-digital">
	<div class="container">
		<h2 class="title section-title">Как мы работаем</h2>
		<div class="services__inner">
			<div class="services__item">
				<div class="service-card">
					<div class="service-card__inner">
						<div class="service-card__top">
							<h3 class="title service-card__title">Аналитика</h3>
							<div class="service-card__icon service-card__icon--support">
							</div>
						</div>
						<div class="service-card__middle">
							<p class="service-card__descr">
								 Проводим предпроектные исследования и глубинные интервью. Выявляем истинные потребности пользователей. Синтезируем полученные данные и готовим задания для проектирования. Осуществляем авторский надзор.
							</p>
						</div>
						<div class="service-card__bottom">
							<ul class="technologies">
								<li class="technologies__item">BRD</li>
								<li class="technologies__item">MTT</li>
								<li class="technologies__item">Interviews</li>
								<li class="technologies__item">CJM</li>
								<li class="technologies__item">Audits</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="services__item">
				<div class="service-card">
					<div class="service-card__inner">
						<div class="service-card__top">
							<h3 class="title service-card__title">UX / UI</h3>
							<div class="service-card__icon service-card__icon--ui">
							</div>
						</div>
						<div class="service-card__middle">
							<p class="service-card__descr">
								 Проектируем интерфейсы основанные на данных и пишем Use-кейсы. Разрабатываем карты пути пользователя. Проводим usability-аудиты.
							</p>
						</div>
						<div class="service-card__bottom">
							<ul class="technologies">
								<li class="technologies__item">Sketch</li>
								<li class="technologies__item">Axure</li>
								<li class="technologies__item">Moqup</li>
								<li class="technologies__item">MindMup</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="services__item">
				<div class="service-card">
					<div class="service-card__inner">
						<div class="service-card__top">
							<h3 class="title service-card__title">Дизайн</h3>
							<div class="service-card__icon service-card__icon--wdesign">
							</div>
						</div>
						<div class="service-card__middle">
							<p class="service-card__descr">
								 Готовим дизайн-концепции. Предоставляем услуги по веб-дизайну, а также работаем в коллаборациях с ведущими студиями страны. Выполняем задачи по дизайн-поддержке проектов.
							</p>
						</div>
						<div class="service-card__bottom">
							<ul class="technologies">
								<li class="technologies__item">Photoshop</li>
								<li class="technologies__item">Illustrator</li>
								<li class="technologies__item">Figma</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="services__item">
				<div class="service-card">
					<div class="service-card__inner">
						<div class="service-card__top">
							<h3 class="title service-card__title">Backend</h3>
							<div class="service-card__icon">
							</div>
						</div>
						<div class="service-card__middle">
							<p class="service-card__descr">
								 Разрабатываем надежный бэкенд с использованием современных CMS и Framework. Пишем собственные API и проводим интеграции с любыми внешними системами. Проектируем базы данных.
							</p>
						</div>
						<div class="service-card__bottom">
							<ul class="technologies">
								<li class="technologies__item">PHP7</li>
								<li class="technologies__item">Laravel</li>
								<li class="technologies__item">Symfony</li>
								<li class="technologies__item">Bitrix</li>
								<li class="technologies__item">Elasticsearch</li>
								<li class="technologies__item">Sphinx</li>
								<li class="technologies__item">XML</li>
								<li class="technologies__item">MySQL</li>
								<li class="technologies__item">RabbitMQ</li>
								<li class="technologies__item">Apache</li>
								<li class="technologies__item">Kafka</li>
								<li class="technologies__item">PostgreSQL</li>
								<li class="technologies__item">Docker</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="services__item">
				<div class="service-card">
					<div class="service-card__inner">
						<div class="service-card__top">
							<h3 class="title service-card__title">Frontend</h3>
							<div class="service-card__icon">
							</div>
						</div>
						<div class="service-card__middle">
							<p class="service-card__descr">
								 Работаем как со статичной версткой, так и современными JS Framework. Создаем SPA. Делаем сложные анимации с оптимальной скоростью загрузки. Адаптируем сайты под различные устройства и разрешения экранов.
							</p>
						</div>
						<div class="service-card__bottom">
							<ul class="technologies">
								<li class="technologies__item">HTML5 + CSS3</li>
								<li class="technologies__item">Sass</li>
								<li class="technologies__item">React</li>
								<li class="technologies__item">Vue</li>
								<li class="technologies__item">Redux</li>
								<li class="technologies__item">Webpack</li>
								<li class="technologies__item">Gulp</li>
								<li class="technologies__item">Canvas</li>
								<li class="technologies__item">WebGL</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="services__item">
				<div class="service-card">
					<div class="service-card__inner">
						<div class="service-card__top">
							<h3 class="title service-card__title">QA</h3>
							<div class="service-card__icon service-card__icon--qa">
							</div>
						</div>
						<div class="service-card__middle">
							<p class="service-card__descr">
								 Тестируем разработанные системы. Улучшаем работу отделов Backend и Frontend. Держим баланс между здоровым перфекционизмом и интересами бизнеса клиента. Составляем прозрачные тест-кейсы и понятные баг-листы.
							</p>
						</div>
						<div class="service-card__bottom">
							<ul class="technologies">
								<li class="technologies__item">Functional</li>
								<li class="technologies__item">Performance</li>
								<li class="technologies__item">Regression</li>
								<li class="technologies__item">Smoke</li>
								<li class="technologies__item">Load</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"cases",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "cases",
		"DETAIL_URL" => "#SERVER_NAME#/#IBLOCK_CODE#/#CODE#/",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"NAME",1=>"SORT",2=>"PREVIEW_TEXT",3=>"PREVIEW_PICTURE",4=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "cases",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"TEAM",1=>"VOLUME",2=>"TERM",3=>"LINK_PROJECT",4=>"SOLUTION_STAK",5=>"CODE",6=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
<div class="page-section reviews">
	<div class="container">
		<h2 class="title section-title">Отзывы</h2>
		<div class="reviews__inner">
			<div class="swiper-container js-reviews-slider swiper-container-initialized swiper-container-horizontal">
				<div class="reviews-slider swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">
					<div class="reviews-slider__item swiper-slide swiper-slide-active" data-id="0" style="width: 660px; margin-right: 20px;">
						<div class="reviews-slider__company-wrapper">
							<div class="reviews-slider__company-info">
								<div class="reviews-slider__name">
									 Евгений Сахаров 0
								</div>
								<div class="reviews-slider__position-wrapper">
									<div class="reviews-slider__position">
										 СТО
									</div>
 <a class="reviews-slider__company" href="#">Сантехника-Онлайн</a>
								</div>
							</div>
							<div class="reviews-popup__logo-wrapper">
 <img src="assets/images/content/partners/logo-san-teh.png" class="reviews-slider__company-logo">
							</div>
						</div>
						<div class="reviews-slider__text">
							 Hawking Bros, благодаря быстрому подключению дополнительных специалистов, всего лишь за месяц увеличила объем вырабатываемых часов в 2 раза.
						</div>
						<div class="reviews-slider__more-info js-show-popup-reviews">
							 Читать отзыв<span class="reviews-slider__arrow"> </span>
						</div>
					</div>
					<div class="reviews-slider__item swiper-slide swiper-slide-next" data-id="1" style="width: 660px; margin-right: 20px;">
						<div class="reviews-slider__company-wrapper">
							<div class="reviews-slider__company-info">
								<div class="reviews-slider__name">
									 Юлия Локшина 1
								</div>
								<div class="reviews-slider__position-wrapper">
									<div class="reviews-slider__position">
										 PR-менеджер
									</div>
 <a class="reviews-slider__company" href="#">Ales Capital </a>
								</div>
							</div>
							<div class="reviews-popup__logo-wrapper">
 <img src="assets/images/content/partners/logo-ales.png" class="reviews-slider__company-logo">
							</div>
						</div>
						<div class="reviews-slider__text">
							 Работа над проектом шла быстро, все правки и пожелания ребята учитывали и быстро вносили. <br>
							 Скорость — это сильная сторона команды.
						</div>
						<div class="reviews-slider__more-info js-show-popup-reviews">
							 Читать отзыв<span class="reviews-slider__arrow"> </span>
						</div>
					</div>
					<div class="reviews-slider__item swiper-slide" data-id="2" style="width: 660px; margin-right: 20px;">
						<div class="reviews-slider__company-wrapper">
							<div class="reviews-slider__company-info">
								<div class="reviews-slider__name">
									 Евгений Сахаров 2
								</div>
								<div class="reviews-slider__position-wrapper">
									<div class="reviews-slider__position">
										 СТО
									</div>
 <a class="reviews-slider__company" href="#">Сантехника-Онлайн</a>
								</div>
							</div>
							<div class="reviews-popup__logo-wrapper">
 <img src="assets/images/content/partners/logo-san-teh.png" class="reviews-slider__company-logo">
							</div>
						</div>
						<div class="reviews-slider__text">
							 Hawking Bros, благодаря быстрому подключению дополнительных специалистов, всего лишь за месяц увеличила объем вырабатываемых часов в 2 раза.
						</div>
						<div class="reviews-slider__more-info js-show-popup-reviews">
							 Читать отзыв<span class="reviews-slider__arrow"> </span>
						</div>
					</div>
					<div class="reviews-slider__item swiper-slide" data-id="3" style="width: 660px; margin-right: 20px;">
						<div class="reviews-slider__company-wrapper">
							<div class="reviews-slider__company-info">
								<div class="reviews-slider__name">
									 Юлия Локшина 3
								</div>
								<div class="reviews-slider__position-wrapper">
									<div class="reviews-slider__position">
										 PR-менеджер
									</div>
 <a class="reviews-slider__company" href="#">Ales Capital</a>
								</div>
							</div>
							<div class="reviews-popup__logo-wrapper">
 <img src="assets/images/content/partners/logo-ales.png" class="reviews-slider__company-logo">
							</div>
						</div>
						<div class="reviews-slider__text">
							 Работа над проектом шла быстро, все правки и пожелания ребята учитывали и быстро вносили. <br>
							 Скорость — это сильная сторона команды.
						</div>
						<div class="reviews-slider__more-info js-show-popup-reviews">
							 Читать отзыв<span class="reviews-slider__arrow"> </span>
						</div>
					</div>
					<div class="reviews-slider__item swiper-slide" data-id="4" style="width: 660px; margin-right: 20px;">
						<div class="reviews-slider__company-wrapper">
							<div class="reviews-slider__company-info">
								<div class="reviews-slider__name">
									 Юлия Локшина 4
								</div>
								<div class="reviews-slider__position-wrapper">
									<div class="reviews-slider__position">
										 СТО
									</div>
 <a class="reviews-slider__company" href="#">Сантехника-Онлайн</a>
								</div>
							</div>
							<div class="reviews-popup__logo-wrapper">
 <img src="assets/images/content/partners/logo-san-teh.png" class="reviews-slider__company-logo">
							</div>
						</div>
						<div class="reviews-slider__text">
							 Hawking Bros, благодаря быстрому подключению дополнительных специалистов, всего лишь за месяц увеличила объем вырабатываемых часов в 2 раза.
						</div>
						<div class="reviews-slider__more-info js-show-popup-reviews">
							 Читать отзыв<span class="reviews-slider__arrow"> </span>
						</div>
					</div>
					<div class="reviews-slider__item swiper-slide" data-id="5" style="width: 660px; margin-right: 20px;">
						<div class="reviews-slider__company-wrapper">
							<div class="reviews-slider__company-info">
								<div class="reviews-slider__name">
									 Юлия Локшина 5
								</div>
								<div class="reviews-slider__position-wrapper">
									<div class="reviews-slider__position">
										 PR-менеджер
									</div>
 <a class="reviews-slider__company" href="#">Ales Capital</a>
								</div>
							</div>
							<div class="reviews-popup__logo-wrapper">
 <img src="assets/images/content/partners/logo-ales.png" class="reviews-slider__company-logo">
							</div>
						</div>
						<div class="reviews-slider__text">
							 Работа над проектом шла быстро, все правки и пожелания ребята учитывали и быстро вносили. <br>
							 Скорость — это сильная сторона команды.
						</div>
						<div class="reviews-slider__more-info js-show-popup-reviews">
							 Читать отзыв<span class="reviews-slider__arrow"> </span>
						</div>
					</div>
					<div class="reviews-slider__item swiper-slide" data-id="6" style="width: 660px; margin-right: 20px;">
						<div class="reviews-slider__company-wrapper">
							<div class="reviews-slider__company-info">
								<div class="reviews-slider__name">
									 Евгений Сахаров 6
								</div>
								<div class="reviews-slider__position-wrapper">
									<div class="reviews-slider__position">
										 СТО
									</div>
 <a class="reviews-slider__company" href="#">Сантехника-Онлайн</a>
								</div>
							</div>
							<div class="reviews-popup__logo-wrapper">
 <img src="assets/images/content/partners/logo-san-teh.png" class="reviews-slider__company-logo">
							</div>
						</div>
						<div class="reviews-slider__text">
							 Hawking Bros, благодаря быстрому подключению дополнительных специалистов, всего лишь за месяц увеличила объем вырабатываемых часов в 2 раза.
						</div>
						<div class="reviews-slider__more-info js-show-popup-reviews">
							 Читать отзыв<span class="reviews-slider__arrow"> </span>
						</div>
					</div>
					<div class="reviews-slider__item swiper-slide" data-id="7" style="width: 660px; margin-right: 20px;">
						<div class="reviews-slider__company-wrapper">
							<div class="reviews-slider__company-info">
								<div class="reviews-slider__name">
									 Юлия Локшина 7
								</div>
								<div class="reviews-slider__position-wrapper">
									<div class="reviews-slider__position">
										 PR-менеджер
									</div>
 <a class="reviews-slider__company" href="#">Ales Capital</a>
								</div>
							</div>
							<div class="reviews-popup__logo-wrapper">
 <img src="assets/images/content/partners/logo-ales.png" class="reviews-slider__company-logo">
							</div>
						</div>
						<div class="reviews-slider__text">
							 Работа над проектом шла быстро, все правки и пожелания ребята учитывали и быстро вносили. <br>
							 Скорость — это сильная сторона команды.
						</div>
						<div class="reviews-slider__more-info js-show-popup-reviews">
							 Читать отзыв<span class="reviews-slider__arrow"> </span>
						</div>
					</div>
				</div>
				<div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets swiper-pagination-bullets-dynamic" style="width: 126px;">
 <span class="swiper-pagination-bullet swiper-pagination-bullet-active swiper-pagination-bullet-active-main" tabindex="0" role="button" aria-label="Go to slide 1" style="left: 36px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-main" tabindex="0" role="button" aria-label="Go to slide 2" style="left: 36px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-main" tabindex="0" role="button" aria-label="Go to slide 3" style="left: 36px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-next" tabindex="0" role="button" aria-label="Go to slide 4" style="left: 36px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-next-next" tabindex="0" role="button" aria-label="Go to slide 5" style="left: 36px;"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 6" style="left: 36px;"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 7" style="left: 36px;"></span>
				</div>
 <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
			</div>
			<div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false">
			</div>
			<div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-disabled="true">
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>