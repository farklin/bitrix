<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// проверка в случае ошибки выдает message, в случае удачи выдает 
function validate($key, string $message, bool $require)
{
    global $result; 
    if(isset($_POST[$key]) and !empty($_POST[$key]))
    {
        return $_POST[$key]; 
    }else{
        if($require){
            $result['message'] = $message;
            return "";
        }else{
            return ""; 
        }
    }
}

if($_SERVER['REQUEST_METHOD'] == "POST"){
    
    $name = validate('name', 'Введите имя |', true); 
    $phone = validate('phone', 'Введите телефон |', true); 
    $email = validate('email', 'Введите email |', true); 
    $message = validate('message', 'Введите сообщение |', false); 

    if(empty($result['message'])){
        CModule::IncludeModule('iblock'); 
    
        $el = new CIBlockElement;

        // $PROP = array();
        // $PROP['PHONE'] = $_POST['phone']; 

        $iblock_id = 9;
        $section_id = false;
        
        $arFields = Array(
        "DATE_CREATE"  => date("d.m.Y H:i:s"), 
        "CREATED_BY"    => $GLOBALS['USER']->GetID(),	
        "IBLOCK_SECTION_ID" => $section_id, 
        "IBLOCK_ID"      => $iblock_id, 
        //"PROPERTY_VALUES"=> $PROP, 
        "NAME"           => strip_tags($name),
        "ACTIVE"         => "Y",
        "PREVIEW_TEXT"   => mb_substr(strip_tags($phone), 0, 100), 
        "DETAIL_TEXT"    => strip_tags($message, '<br></br><p>'), 
        );

        if($ID = $el->Add($arFields)){
            $result['status'] = 'success';
            $result['message'] = 'Форма успешно добавлена';
        }		 
        else{
            $result['status'] = 'error';
            $result['message'] = $el->LAST_ERROR;
        }

        
    }else{
        $result['status'] = 'error';
    }

}else{
    $result['status'] = 'error';
    $result['message'] = 'Все поля обязательны для заполнения';
}



echo json_encode($result);
?>



<?die() ;?>
