<? 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION; 

$aMenuLinksExt = $APPLICATION->IncludeComponent(
	"bitrix:menu.sections",
	"",
	Array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"DEPTH_LEVEL" => "3",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "all",
		"ID" => $_REQUEST["ID"],
		"IS_SEF" => "N",
		"SECTION_URL" => "#SERVER_NAME#/#IBLOCK_CODE#/"
	)
);


$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>