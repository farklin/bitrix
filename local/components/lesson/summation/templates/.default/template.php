<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?php if (empty($arResult['SUM'])) { ?>
      <form action="">
            <div>
                  <label for="number1"><?php echo Loc::getMessage("LABEL_NUMBER1"); ?></label>
                  <input type="number" id="number1" name="number1">
            </div>
            +
            <div>
                  <label for="number2"><?php echo Loc::getMessage("LABEL_NUMBER2"); ?></label>
                  <input type="number" name="number2">
            </div>
            <button><?php echo Loc::getMessage("BUTTON"); ?></button>
      </form>
<?php } else {
      echo $arResult['SUM'];
} ?>

<?php
use Bitrix\Main\Diag\Debug;

$record = Application::getConnection()
->query("select IBLOCK_ID, PREVIEW_TEXT, DETAIL_TEXT from b_iblock_element where IBLOCK_ID = 1")
->fetch();
// echo '<pre>'; 
// print_r($record);
// echo '</pre>'; 

echo '<table>'; 
echo '<tr>'; 
foreach($record as $key => $value)
{     
      if(!is_array($record[$key]))
      {     
            echo '<td>'; 
            print_r($record[$key]); 
            echo '</td>'; 
      }
    
}
echo '</tr>'; 

echo '</table>'; 