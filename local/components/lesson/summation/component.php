<?php
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Server; 
use Bitrix\Main\Context; 

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$server = Context::getCurrent()->getServer(); 
$request = Application::getInstance()->getContext()->getRequest();

$sum = 0; 
if($server->getRequestMethod() == 'GET' and !empty($request->getQuery("number1")) and !empty($request->getQuery("number2")) )
{   
    $number1 = htmlspecialchars($request->getQuery("number1"));
    $number2 = htmlspecialchars($request->getQuery("number2"));

    $sum = $number1 + $number2;
}

$arResult['SUM'] = $sum; 
$this->IncludeComponentTemplate();
?>