<footer class="footer">
    <div class="container">
        <div class="footer__inner">
            <div class="footer__row">
                <div class="footer__logo"><a class="logo" href="/">
                        <div class="logo__pic">
                            <svg class="icon logo">
                                <use xlink:href="#logo"></use>
                            </svg>
                        </div>
                    </a></div>
            </div>
            <div class="footer__row"><a class="footer__privacy link--privacy" href="sogl.pdf" target="_blank">Политика конфиденциальности</a><span class="footer__copyright">© 2014 - 2019 Hawking Brothers</span></div>
        </div>
    </div>
</footer>
<div class="cookie js-cookie">
    <div class="container">
        <div class="cookie__inner"><span class="cookie__text">Мы используем файлы cookie, чтобы улучшить работу сайта. Дальнейшее пребывание на сайте означает согласие с их применением. <a href="#">Узнать подробнее</a></span>
            <div class="cookie__button button js-cookie-button">Принять</div>
        </div>
    </div>
</div>
</div>
</div>


<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"left-menu",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "podmenu",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(""),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "left",
		"USE_EXT" => "Y"
	)
);?>
<!-- <div class="main-menu-holder">
    <div class="container">
        <div class="main-menu-box">
            <div class="js-nav-menu nav-menu">
                <div class="nav-menu__head">
                    <div class="nav-menu__head-inner">
                        <div class="mode-icon js-switch-theme nav-menu__mode">
                            <svg class="icon night-mode">
                                <use xlink:href="#night-mode"></use>
                            </svg>
                            <svg class="icon day-mode">
                                <use xlink:href="#day-mode"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="nav-menu__wrap">
                    <div class="nav-menu__inner js-menu-inner">
                        <nav class="main-menu nav-menu__menu menu-top--animate">
                            <ul class="main-menu__list">
                                <li class="main-menu__item"><a class="main-menu__link link--red js-link-services" href="index.html">Главная</a></li>
                                <li class="main-menu__item"><a class="main-menu__link link--red" href="about.html">О компании</a><a class="main-menu__sublink link--red js-menu-anchor" href="about.html#reviews">Отзывы</a><a class="main-menu__sublink link--red js-menu-anchor" href="about.html#publications">Публикации</a><a class="main-menu__sublink link--red js-menu-anchor" href="page-articles.html">Статьи</a></li>
                                <li class="main-menu__item"><a class="main-menu__link link--red js-link-services js-menu-anchor" href="/#services-main">Услуги</a><a class="main-menu__sublink link--red" href="digital.html">Digital-разработка</a><a class="main-menu__sublink link--red" href="up-team.html">Усиление IT-команд</a></li>
                                <li class="main-menu__item"><a class="main-menu__link link--red" href="cases.html">Наши проекты</a></li>
                                <li class="main-menu__item"><a class="main-menu__link link--red" href="career.html">Вакансии</a><a class="main-menu__sublink link--red" href="about.html" target="_blank">Стажировки</a><a class="main-menu__sublink link--red js-menu-anchor" href="career.html#career-culture">Корпоративная культура</a></li>
                                <li class="main-menu__item"><a class="main-menu__link link--red js-link-contacts js-menu-anchor" href="/#contacts">Контакты</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->


<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "cases-popup",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("NAME", "SORT", "PREVIEW_TEXT", "PREVIEW_PICTURE", ""),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "7",
        "IBLOCK_TYPE" => "cases",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "20",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array("CODE", "TEAM", "VOLUME", "TERM", "LINK_PROJECT", "SOLUTION_STAK", ""),
        "SET_BROWSER_TITLE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
    )
); ?>

<script defer type="text/javascript">
    function isAnyPartOfElementInViewport(el) {

        var rect = el.getBoundingClientRect();
        // DOMRect { x: 8, y: 8, width: 100, height: 100, top: 8, right: 108, bottom: 108, left: 8 }
        var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
        var windowWidth = (window.innerWidth || document.documentElement.clientWidth);

        // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
        var vertInView = (rect.top <= windowHeight + 1000) && ((rect.top + rect.height) >= 0);
        var horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

        return (vertInView && horInView);
    }
</script>
<script defer type="text/javascript">
    window.addEventListener('scroll', function() {
        var ymap = document.querySelector('.contacts');
        if (isAnyPartOfElementInViewport(ymap) && mapLoaded === false) {
            var elem = document.createElement('script');
            elem.type = 'text/javascript';
            elem.src = '/js/ymaps-touch-scroll.js';
            document.getElementsByTagName('body')[0].appendChild(elem);

        } else return;
    });
</script>
<script defer type="text/javascript">
    var mapLoaded = false
    window.addEventListener('scroll', function() {
        var ymap = document.querySelector('.contacts');
        if (isAnyPartOfElementInViewport(ymap) && mapLoaded === false) {
            yandexMapCreate();
            mapLoaded = true;
        } else return;
    });



    function yandexMapCreate() {
        var elem = document.createElement('script');
        elem.type = 'text/javascript';
        elem.src = '//api-maps.yandex.ru/2.1/?apikey=1b577d37-f341-456a-8734-b6a76134da85&lang=ru_RU&onload=yandexMapInit';
        document.getElementsByTagName('body')[0].appendChild(elem);
    }

    function yandexMapInit() {
        var mapContainer = document.querySelector('.map');
        if (mapContainer === null) {
            return;
        }

        var yMap = new ymaps.Map(mapContainer, {
            center: [56.13768336653939, 40.374368568354896],
            zoom: 16,
            controls: ['zoomControl', 'routeButtonControl']
        });
        var myGeoObject = new ymaps.GeoObject({
            geometry: {
                type: "Point",
                coordinates: [56.13768336653939, 40.374368568354896]
            }
        }, {
            iconColor: '#eb5757'
        });
        yMap.geoObjects.add(myGeoObject);
        ymapsTouchScroll(yMap);
    }
</script>
</body>

</html>