<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult)) return;

$arMenu = array();
$first = true;
foreach ($arResult as $itemIndex => $arItem) {
	if ($arItem["PERMISSION"] > "D" && $arItem["DEPTH_LEVEL"] == 1) {
		$className = '';
		if ($first) {
			$className .= ' first-item';
			$first = false;
		}
		if ($arItem['SELECTED']) {
			$className .= ' selected';
		}

		$arItem['CLASS'] = $className;
		$arMenu[] = $arItem;
	}
}

if (empty($arMenu)) return;

$arMenu[count($arMenu) - 1]['CLASS'] .= ' last-item';
?>
<div class="main-menu-holder">
	<div class="container">
		<div class="main-menu-box">
			<div class="js-nav-menu nav-menu">
				<div class="nav-menu__head">
					<div class="nav-menu__head-inner">
						<div class="mode-icon js-switch-theme nav-menu__mode">
							<svg class="icon night-mode">
								<use xlink:href="#night-mode"></use>
							</svg>
							<svg class="icon day-mode">
								<use xlink:href="#day-mode"></use>
							</svg>
						</div>
					</div>
				</div>
				<div class="nav-menu__wrap">
					<div class="nav-menu__inner js-menu-inner">
						<nav class="main-menu nav-menu__menu menu-top--animate">
							<ul class="main-menu__list">
								<?php foreach ($arResult as $itemResult) { ?>
									<?php if ($itemResult['DEPTH_LEVEL'] == 1) { ?>
										<li class="main-menu__item"><a class="main-menu__link link--red js-link-services" href="<?= $itemResult['LINK'] ?>"><?= $itemResult['TEXT'] ?></a></li>
									<?php } ?>
									<?php if ($itemResult['DEPTH_LEVEL'] == 2) { ?>
										<a class="main-menu__sublink link--red js-menu-anchor" href="<?= $itemResult['LINK'] ?>"><?= $itemResult['TEXT'] ?></a>
									<?php } ?>
								<?php } ?>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- 

<div class="main-menu-holder">
	<div class="container">
		<div class="main-menu-box">
			<div class="js-nav-menu nav-menu">
				<div class="nav-menu__head">
					<div class="nav-menu__head-inner">
						<div class="mode-icon js-switch-theme nav-menu__mode">
							<svg class="icon night-mode">
								<use xlink:href="#night-mode"></use>
							</svg>
							<svg class="icon day-mode">
								<use xlink:href="#day-mode"></use>
							</svg>
						</div>
					</div>
				</div>
				<div class="nav-menu__wrap">
					<div class="nav-menu__inner js-menu-inner">
						<nav class="main-menu nav-menu__menu menu-top--animate">
							<ul class="main-menu__list">
								<li class="main-menu__item"><a class="main-menu__link link--red js-link-services" href="index.html">Главная</a></li>
								<li class="main-menu__item"><a class="main-menu__link link--red" href="about.html">О компании</a><a class="main-menu__sublink link--red js-menu-anchor" href="about.html#reviews">Отзывы</a><a class="main-menu__sublink link--red js-menu-anchor" href="about.html#publications">Публикации</a><a class="main-menu__sublink link--red js-menu-anchor" href="page-articles.html">Статьи</a></li>
								<li class="main-menu__item"><a class="main-menu__link link--red js-link-services js-menu-anchor" href="/#services-main">Услуги</a>
									<a class="main-menu__sublink link--red" href="digital.html">Digital-разработка</a>
									<a class="main-menu__sublink link--red" href="up-team.html">Усиление IT-команд</a>
								</li>
								<li class="main-menu__item"><a class="main-menu__link link--red" href="cases.html">Наши проекты</a></li>
								<li class="main-menu__item"><a class="main-menu__link link--red" href="career.html">Вакансии</a><a class="main-menu__sublink link--red" href="about.html" target="_blank">Стажировки</a><a class="main-menu__sublink link--red js-menu-anchor" href="career.html#career-culture">Корпоративная культура</a></li>
								<li class="main-menu__item"><a class="main-menu__link link--red js-link-contacts js-menu-anchor" href="/#contacts">Контакты</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->