<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<section class="page-section partners">
	<div class="partners-slider">
		<div class="swiper-container js-partners-swiper">
			<ul class="partners__inner swiper-wrapper">
				<? foreach ($arResult["ITEMS"] as $arItem) : ?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
					<li id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class=" partners__item swiper-slide">
						<a class="partner-logo partner-logo--<?= $arItem["NAME"] ?> " href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>" target="_blank">
							<?php if (!empty($arItem['PREVIEW_PICTURE']['SRC'])) : ?>
								<img class="partners__img partners__img--color partners__img--dark" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="">
								<img class="partners__img partners__img--color partners__img--light" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="">
								<img class="partners__img partners__img--gray" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="">
							<?php else : ?>
								<img src="<?= CFile::GetPath($arItem["PROPERTIES"]['SVG']['VALUE']) ?>">
							<?php endif; ?>
						</a>
					</li>
				<? endforeach; ?>
			</ul>
			<div class="swiper-scrollbar"></div>
		</div>
	</div>
</section>