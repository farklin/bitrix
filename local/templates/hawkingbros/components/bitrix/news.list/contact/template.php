<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/contact-form.js"); 
?>


<section class="page-section contacts" id="contacts">
	<div class="container">
		<h2 class="title section-title"><?=$arResult["PAGER_TITLE"]?></h2>
		<div class="contacts__inner">
		<?php $arItem = $arResult["ITEMS"][0]; ?>
			<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
				
			<div id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="contacts__map-holder js-map">
				

					<div class="contacts__map-overlay">
						<div class="contact-info">
							<div class="contact-info__row">
								<span class="title contact-info__title">Email</span><a class="contact-info__link" href="mailto:<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a>
							</div>
							<div class="contact-info__row">
								<span class="title contact-info__title"><?=$arItem["DISPLAY_PROPERTIES"]["PHONE"]["NAME"]?></span>
								<?php foreach($arItem["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"] as $phone): ?> 
									<a class="contact-info__link contact-info__phone" href="tel:<?=$phone?>"><?=$phone?></a>
								<?php endforeach ?> 
							</div>
							<div class="contact-info__row">
								<span class="title contact-info__title"><?=$arItem["DISPLAY_PROPERTIES"]["SOCIAL"]["NAME"]?></span>
								<?php foreach($arItem["DISPLAY_PROPERTIES"]["SOCIAL"]["VALUE"] as $key=>$social): ?> 
									<a class="contact-info__link" href="<?=$arItem["DISPLAY_PROPERTIES"]["SOCIAL"]["DESCRIPTION"][$key]?>" target="_blank"><?=$social?></a>
								<?php endforeach ?> 
							</div>
							<?php foreach($arItem["DISPLAY_PROPERTIES"]["ADRES"]["VALUE"] as $key=>$adres): ?> 
							<div class="contact-info__row">
								<?php if($key == 0):?> 
								<span class="title contact-info__title"><?=$arItem["DISPLAY_PROPERTIES"]["ADRES"]["NAME"]?></span>
								<?php endif ?> 
								<span class="contact-info__text contact-info__city contact-info__text--red js-contact-info-radio-text"><?=$adres?></span>
							
								<span class="contact-info__text contact-info__text--small"><?=$arItem["DISPLAY_PROPERTIES"]["ADRES"]["DESCRIPTION"][$key]?></span>
							</div>
							<?php endforeach ?> 


							<div class="contact-info__row">
								<div class="button contact-info__button js-open-contact-form">
									Написать нам
								</div>
							</div>
						</div>
					</div>
					<div class="map contacts__map">
						<!--.map__close.js-close-map+svg('cross')
			-->
					</div>
			</div>


			<div class="contacts__form-holder js-form-holder">
				<div class="contacts__form-wrap">
					<div class="contact-form__succes-message js-form-succes">
						<div class="contact-form__succes-inner">
							<span class="title">Форма успешно отправлена.</span>
						</div>
					</div>
					<form class="contact-form" name="sendmail" action='/form/' method='post'>
						<div class="contact-form__row">
							<span class="title contact-form__title">Свяжитесь с нами</span>
							<div class="contact-form__close js-close-contact-form">
							</div>
						</div>
						<div class="contact-form__row contact-form__row--name">
							<input class="contact-form__input contact-form__field js-input js-input-name" type="text" name="name" id="name" required="">
							<div class="contact-form__fill-line">
							</div>
							<label class="contact-form__label js-form-label" for="name">Как к вам обращаться?</label>
						</div>
						<div class="contact-form__row">
							<label class="contact-form__label js-form-label" for="phone">Телефон</label> <input class="contact-form__input contact-form__field js-input js-phone customPhone" type="tel" name="phone" id="phone" required="">
							<div class="contact-form__fill-line">
							</div>
						</div>
						<div class="contact-form__row">
							<label class="contact-form__label js-form-label" for="email">Электропочта</label> <input class="contact-form__input contact-form__field js-input customEmail" type="email" name="email" id="email" required="">
							<div class="contact-form__fill-line">
							</div>
						</div>
						<div class="contact-form__row contact-form__flags">
							<input class="contact-form__checkbox" type="radio" id="digital" name="form" checked=""> <label class="contact-form__checkbox-label" for="digital">Digital-разработка</label> <input class="contact-form__checkbox" type="radio" id="up-team" name="form"> <label class="contact-form__checkbox-label" for="up-team">Усиление команды</label>
						</div>
						<div class="contact-form__row contact-form__row--message">
							<label class="contact-form__label js-form-label" for="message">Опишите вашу задачу</label> <textarea class="contact-form__input contact-form__field contact-form__input--textarea contact-form__input--textarea_scroll js-input" name="message" id="message"></textarea>
							<div class="contact-form__fill-line">
							</div>
						</div>
						<div class="contact-form__row contact-form__row--button">
							<button class="contact-form__input button" type="submit">Отправить</button>
						</div>
						<div class="contact-form__row contact-form__row--terms">
							<span class="contact-form__terms">Нажимая "Отправить" Вы соглашаетесь с&nbsp;</span><a class="link--privacy" href="sogl.pdf" target="_blank">Политикой обработки персональных данных</a>
						</div>
						<div class="form-error-container">
						</div>
						<input class="contact-form__person js-input-person" type="text" name="person">
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
