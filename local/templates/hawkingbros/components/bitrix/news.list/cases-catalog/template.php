<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->RestartBuffer();
?>
<section class="page-section cases">
	<div class="container">
		<div class="cases__title-box">
			<h2 class="title section-title link--arrow"> <a href="cases.html">Наши проекты
					<div class="arrow-right title__arrow title__arrow--cases">
						<div class="arrow-right__main">
							<style>
								.cls-1 {
									fill-rule: evenodd;
								}
							</style> <span id="Trash" class="arrow-right__head arrow-right__part"> <span id="Logo-Copy-12"> <span id="ic_arrow_right-copy" data-name="ic arrow right-copy"> <span id="Shape"> <span class="cls-1" d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z" transform="translate(-145.99 -484.4)"> </span></span> </span> </span> </span> <span id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part"> <span id="Logo-Copy-12-2" data-name="Logo-Copy-12"> <span id="ic_arrow_right-copy-2" data-name="ic arrow right-copy"> <span id="Shape-2" data-name="Shape"> <span class="cls-1" d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z" transform="translate(-145.99 -484.4)"> </span></span> </span> </span> </span>
							<div class="arrow-right__fill-wrap">
								<div class="arrow-right__fill">
									<style>
										.cls-1 {
											fill-rule: evenodd;
										}
									</style> <span id="Trash" class="arrow-right__head arrow-right__part"> <span id="Logo-Copy-12"> <span id="ic_arrow_right-copy" data-name="ic arrow right-copy"> <span id="Shape"> <span class="cls-1" d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z" transform="translate(-145.99 -484.4)"> </span></span> </span> </span> </span> <span id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part"> <span id="Logo-Copy-12-2" data-name="Logo-Copy-12"> <span id="ic_arrow_right-copy-2" data-name="ic arrow right-copy"> <span id="Shape-2" data-name="Shape"> <span class="cls-1" d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z" transform="translate(-145.99 -484.4)"> </span></span> </span> </span> </span>
								</div>
							</div>
						</div>
					</div>
				</a>
			</h2>
		</div>
		<div class="cases__filter">
			<form class="filter js-filter" action="/projects/filter.php" method="GET">
				<?php $rsResult = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arResult['ID']), array("PROPERTY_SOLUTION_STAK")); ?>
				<div class="filter__line">
					<?php
					while ($item = $rsResult->Fetch()) : ?>
						<label class="filter-item">
							<input class="filter-item__input js-filter-item__input" type="checkbox" value="<?= $item["PROPERTY_SOLUTION_STAK_VALUE"] ?>" name="solution_stak"><span class="filter-item__text"><?= $item["PROPERTY_SOLUTION_STAK_VALUE"] ?>
								<svg class="icon cross">
									<use xlink:href="#cross"></use>
								</svg></span>
						</label>
					<?php endwhile ?>
				</div>
				<div class="filter__line">
				<?php 
					$rsResult = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arResult['ID']), array("PROPERTY_SERVICE")); 
					while ($item = $rsResult->Fetch()) : 
						$res = CIBlockElement::GetByID($item['PROPERTY_SERVICE_VALUE']);
						if($service = $res->GetNext()){
						?>					
						<label class="filter-item">
							<input class="filter-item__input js-filter-item__input" type="checkbox" value="<?= $service["ID"] ?>" name="service"><span class="filter-item__text"><?= $service["NAME"] ?>
								<svg class="icon cross">
									<use xlink:href="#cross"></use>
								</svg></span>
						</label>
						<?php } ?> 
					<?php endwhile ?>
				</div>
			</form>
		</div> 
		<div class="cases__inner">
			<div class="portfolio">
				<?php foreach ($arResult["ITEMS"] as $arItem) : ?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
					<div id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="portfolio__item">
						<div class="case-card picture js-lazy js-case-card" data-src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
							<div class="case-card__inner">
								<div class="case-card__content-wrap">
									<div class="case-card__content">
										<div class="case-card__content-row">
											<h3 class="title case-card__title"> <a class="js-open-project" href="<?= $arItem["CODE"] ?>"><?= $arItem["NAME"] ?></a></h3>
										</div>
										<div class="case-card__content-row">
											<? if (!empty($arItem['DISPLAY_PROPERTIES']["SOLUTION_STAK"])) : ?>
												<ul class="technologies">
													<?php foreach ($arItem['DISPLAY_PROPERTIES']["SOLUTION_STAK"]["VALUE"] as $solution_stack) : ?>
														<li class="technologies__item"><?= $solution_stack ?> </li>
													<?php endforeach; ?>
												</ul>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<a class="case-card__overlay js-open-project" href="<?= $arItem["CODE"] ?>" target="_blank"></a>
						</div>
					</div>

				<?php endforeach; ?>
			</div>
		</div>
	</div>
</section>