<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="services-main" id="services-main">
	<div class="container">
		<h2 class="title section-title">Услуги</h2>
		<div class="services-main__inner">
			<?php foreach ($arResult["ITEMS"] as $arItem) : ?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="services-main__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
					<div class="service-main-card">
						<div class="service-card-main__inner">
							<div class="service-card__top">
								<h2 class="title link--arrow service-main-card__title"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"> <?= $arItem["NAME"] ?>
										<div class="arrow-right title__arrow service-main-card__arrow">
											<div class="arrow-right__main">
												<style>
													.cls-1 {
														fill-rule: evenodd;
													}
												</style> <span id="Trash" class="arrow-right__head arrow-right__part"> <span id="Logo-Copy-12"> <span id="ic_arrow_right-copy" data-name="ic arrow right-copy"> <span id="Shape"> <span class="cls-1" d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z" transform="translate(-145.99 -484.4)"> </span></span> </span> </span> </span> <span id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part"> <span id="Logo-Copy-12-2" data-name="Logo-Copy-12"> <span id="ic_arrow_right-copy-2" data-name="ic arrow right-copy"> <span id="Shape-2" data-name="Shape"> <span class="cls-1" d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z" transform="translate(-145.99 -484.4)"> </span></span> </span> </span> </span>
												<div class="arrow-right__fill-wrap">
													<div class="arrow-right__fill service-main-card__arrow-fill">
														<style>
															.cls-1 {
																fill-rule: evenodd;
															}
														</style> <span id="Trash" class="arrow-right__head arrow-right__part"> <span id="Logo-Copy-12"> <span id="ic_arrow_right-copy" data-name="ic arrow right-copy"> <span id="Shape"> <span class="cls-1" d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z" transform="translate(-145.99 -484.4)"> </span></span> </span> </span> </span> <span id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part"> <span id="Logo-Copy-12-2" data-name="Logo-Copy-12"> <span id="ic_arrow_right-copy-2" data-name="ic arrow right-copy"> <span id="Shape-2" data-name="Shape"> <span class="cls-1" d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z" transform="translate(-145.99 -484.4)"> </span></span> </span> </span> </span>
													</div>
												</div>
											</div>
										</div>
									</a></h2>
							</div>
							<div class="service-main-card__middle">
								<p class="service-main-card__descr">
									<?= $arItem['PREVIEW_TEXT']; ?>
								</p>
							</div>
							<? if (!empty($arItem['DISPLAY_PROPERTIES']["TYPES"])) : ?>

								<div class="service-main-card__bottom">
									<ul class="what-do">
										<?php foreach ($arItem['DISPLAY_PROPERTIES']["TYPES"]["VALUE"] as $type) : ?>
											<li class="what-do__item"><?= $type ?></li>
										<?php endforeach; ?>
									</ul>
									<img src="<?= CFile::GetPath($arItem["PROPERTIES"]['SVG']['VALUE']) ?>" class="service-main-card__icon">
								</div>

							<?php endif; ?>

						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>