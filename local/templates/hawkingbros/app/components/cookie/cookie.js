function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {

  options = options || {
    path: '/',
    'max-age': 60*60*24*30
  }

  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }

  var updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (var optionKey in options) {
    updatedCookie += "; " + optionKey;
    var optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}

$(document).ready(function() {
  if(!getCookie('hideCookie')) {
    $('.js-cookie').show();
  }
  $(document).on('click', '.js-cookie-button', function() {
    $('.js-cookie').fadeOut();
    setCookie('hideCookie', 'true');
  });
});