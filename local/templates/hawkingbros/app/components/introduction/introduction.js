
$(function() {
    $('.js-anchor').on('click', function(event) {
        event.preventDefault();

        var link = $(this).attr('href');
        var $targetElement = $(link);

        if ($targetElement.length) {
            var scrolled = window.pageYOffset;
            var offsetTop = $(link).offset().top;

            if(scrolled > offsetTop) {
                offsetTop -= $('.header').innerHeight();
            }

            $('html, body').animate({scrollTop:offsetTop},600)
        }
    });
});