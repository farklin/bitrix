$(function() {
  
  if ($('.js-corp-portals-partners-swiper').length) {
    var partnersSwiperOption = {
      speed: 400,
      spaceBetween: 80,
      slidesPerView: 1.5,
      slidesPerColumn: 2,
      slidesPerColumnFill: 'row',
      // slidesPerGroup: 3,
      // centeredSlides: true,
      // centeredSlidesBounds: true,
      scrollbar: {
        el: '.swiper-scrollbar',
      },
    }
  
    var partnersSwiperActive = false;
    var partnersSwiper;
  
    if ($(window).innerWidth() < 768) {
      partnersSwiper = new Swiper('.js-corp-portals-partners-swiper', partnersSwiperOption);
      partnersSwiperActive = true;
    }
  
    $(window).on('resize', function() {
      
      if ($(this).innerWidth() >767 && partnersSwiperActive) {
        partnersSwiper.destroy(1, 1);
        partnersSwiperActive = false;
      } else if($(this).innerWidth() < 768 && partnersSwiperActive === false) {
        partnersSwiper = new Swiper('.js-corp-portals-partners-swiper', partnersSwiperOption);
        partnersSwiperActive = true;
      } 
  
    })

  }

  

})
