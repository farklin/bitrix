$(document).ready(function() {

  var reviewsSwiper = new Swiper('.js-reviews-slider', {
    speed: 400,
    slidesPerView: 1 ,
    roundLengths: true,
    spaceBetween: 20,
    watchOverflow: true,
    navigation: {
      nextEl: '.js-reviews-slider~.swiper-button-next',
      prevEl: '.js-reviews-slider~.swiper-button-prev',
    },
    pagination: {
      el: '.js-reviews-slider .swiper-pagination',
      dynamicBullets: true,
      dynamicMainBullets: 3,
      clickable:true
    },
    breakpoints:{
      900: {
        slidesPerView: 2,
        effect: 'fade'
      }
    }
  });

  var popupSwiper = new Swiper('.js-reviews-popup-slider', {
    roundLengths: true,
    spaceBetween: 40,
    watchOverflow: true,
    effect: 'fade',
    navigation: {
      nextEl: '.js-reviews-popup-slider~.swiper-button-next',
      prevEl: '.js-reviews-popup-slider~.swiper-button-prev',
    },
    pagination: {
      el: '.js-reviews-popup-slider .swiper-pagination',
      dynamicBullets: true,
      dynamicMainBullets: 3
    },
    allowTouchMove: false,
    keyboard: {
      enabled: true
    }
  });


  popupSwiper.on('slideChange' , function(){
    updatePopupPadding();
    $('.reviews-popup .reviews-popup__text').scrollTop(0);
    $('.reviews-popup .reviews-popup__text').trigger('ps-y-reach-start');
  }); 


  $('.reviews-popup__text').each(function(index, element) {
    var popupPs = new PerfectScrollbar(element, {
      wheelSpeed: 0.5,
      wheelPropagation: true,
      minScrollbarLength: 10,
      maxScrollbarLength: 15
    });
  
    popupPs.update();
    $(window).on('resize', function() {
      popupPs.update();
    });
    $(element).on('ps:update', function() {
      popupPs.update();
    });

  });

  $(document).on('click' , '.js-show-popup-reviews' , function(){

    var sliderNumber = $(this).closest('.swiper-slide').attr('data-id');
    sliderNumber = parseInt(sliderNumber);

    $('.reviews-popup').addClass('is-show');
    $('body').css({'overflow':'hidden'})
    
    popupSwiper.slideTo(sliderNumber, 1000 , false );
    popupSwiper.update();
    updatePopupPadding();
    $('.reviews-popup .reviews-popup__text').trigger('ps:update');
    $('.reviews-popup .reviews-popup__text').scrollTop(0);
    $('.reviews-popup .reviews-popup__text').trigger('ps-y-reach-start');
  });

  $('.js-hide-popup-reviews').on('click' , function(){
    $('.reviews-popup').removeClass('is-show');
    $('.reviews-popup').css('position' , 'fixed');
    $('body').css({'overflow':'auto'})
  });

  $('.reviews-popup__text').on('ps-scroll-y' , function(){
    $(this).closest('.reviews-popup__text-wrapper').removeClass('is-top');
    $(this).closest('.reviews-popup__text-wrapper').removeClass('is-bottom');
  });

  $('.reviews-popup__text').on('ps-y-reach-start' , function(){
    $(this).closest('.reviews-popup__text-wrapper').addClass('is-top');
  });

  $('.reviews-popup__text').on('ps-y-reach-end' , function(){
    $(this).closest('.reviews-popup__text-wrapper').addClass('is-bottom');
  });

  $(document).on('click' , 'swiper-button-next' , 'swiper-button-prev' , function(){
    $('.reviews-popup').find('.ps__rail-y').css('opacity' , '0' , '!important');
  });

  function updatePopupPadding() {
    $('.reviews-popup__inner').each(function() {
      var $info = $(this).find('.reviews-popup__info'),
          $text = $(this).find('.reviews-popup__text-inner'),
          $wrapper = $(this).find('.reviews-popup__text-wrapper');
      $text.css('padding-top', $info.innerHeight() + 'px');
      $wrapper.css({
        'top': $info.innerHeight() + 'px',
        'margin-top': -$info.innerHeight()*2 + 'px'
      });
    });
  }
  updatePopupPadding();
  $(window).on('resize', function() {
    updatePopupPadding();
  });
});