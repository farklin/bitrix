$(function () {
  var $consultationForm = $('.js-consultation-form'),
      $errorContainer = $consultationForm.find('.form-error-container'),
      $hiddenField = $consultationForm.find('.js-input-person');

  $consultationForm.validate({
    ignore: [],
    errorLabelContainer: $errorContainer,
    submitHandler: function () {
      if ($hiddenField.val() === '') {
        var data = new FormData();

        $consultationForm.find('.js-input').each(function () {
          var value = $(this).val();

          if (Array.isArray(value)) {
            value = value.join(', ')
          }

          data.append(this.name, value);
        });

        $.ajax({
          url: "/sendfeedback",
          type: "POST",
          data: data,
          processData: false,
          contentType: false,
          success: function () {
            $consultationForm.trigger('formSubmit');
          }
        });
      }

      return false;
    },
    invalidHandler: function (event, validator) {
      setTimeout(function () {
        $('.error').siblings('.contact-form__fill-line').addClass('error');
      }, 100)
    },
    onkeyup: function (element) {
      $(element).valid();
    },
  })
})