var initSwiper = function(swiperSelector) {
  var partnersSwiperOption = {
    updateOnWindowResize: true,
    speed: 400,
    slidesPerView: 'auto',
    slidesPerColumnFill: 'row',
    scrollbar: {
      el: '.swiper-scrollbar',
    },
    allowTouchMove: false,
  }
  if (window.innerWidth < 768) {
    partnersSwiperOption.loopedSlides = 3;
    partnersSwiperOption.allowTouchMove = true;
  }

  return new Swiper('.js-partners-swiper', partnersSwiperOption);
}

$(function() {
  var swiperSelector = '.js-partners-swiper';
  var innerSwiperSelector = '.partners__inner.swiper-wrapper';
  var partnersSwiper = initSwiper(swiperSelector);
  
  var swiperWidth;
  
  if (!$(swiperSelector).length) return;

  function getSwiperWidth() {
    if(swiperWidth) {
      return swiperWidth;
    } else {
      swiperWidth = 0;
      for(key = 0; key < partnersSwiper.slidesSizesGrid.length; key++) {
        swiperWidth += partnersSwiper.slidesSizesGrid[key];
      }
      //document.querySelector(innerSwiperSelector).style.transform = "translateX(" + (-parseInt((swiperWidth - window.innerWidth) / 2)) + "px)";
    }

    return swiperWidth;
  }

  function swiperMouseTrigger(event) {
    var windowWidth = window.innerWidth - 20;
    var centerX = parseInt(window.innerWidth/2);
  
    var swiperWrapper = document.querySelector(innerSwiperSelector);
    var swiperWidth = getSwiperWidth();
  
    var mouseX = event.clientX;
    var diffX = mouseX-centerX;
  
    var marginsOuterOfWindow = swiperWidth-windowWidth;
    var marginLeft = marginsOuterOfWindow/2 + diffX/windowWidth*marginsOuterOfWindow;
    swiperWrapper.style.transform = "translateX(" + (-parseInt(marginLeft)) + "px)";
  }
  
  $(window).on('resize', function() {
    if (window.innerWidth > 767) {
      partnersSwiper.params.loopedSlides = 7;
      partnersSwiper.allowTouchMove = false;

      partnersSwiper.update();
      swiperWidth = false;

      document.addEventListener('mousemove', swiperMouseTrigger);
    } else {
      document.removeEventListener('mousemove', swiperMouseTrigger);
      partnersSwiper.params.loopedSlides = 3;
      partnersSwiper.allowTouchMove = true;

      partnersSwiper.update();
      swiperWidth = false;

      document.querySelector(innerSwiperSelector).style.transform = "translateX(-510px)";
    }
  })

  if(window.innerWidth > 767) {
    document.addEventListener('mousemove', swiperMouseTrigger);
  } else {
    document.querySelector(innerSwiperSelector).style.transform = "translateX(-510px)";
  }

  getSwiperWidth();
})