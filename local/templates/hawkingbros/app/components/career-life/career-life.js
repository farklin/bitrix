$(function() {

  if ($('.js-life-swiper').length) {
    var lifeSwiperOption = {
      speed: 400,
      spaceBetween: 10,
      slidesPerView: 1,
      scrollbar: {
        el: $('.js-life-scrollbar'),
      },
    }
  
    var lifeSwiperActive = false;
    var lifeSwiper;
  
    if ($(window).innerWidth() < 768) {
      lifeSwiper = new Swiper('.js-life-swiper', lifeSwiperOption);
      lifeSwiperActive = true;
    }
  
    $(window).on('resize', function() {
      
      if ($(this).innerWidth() >767 && lifeSwiperActive) {
        lifeSwiper.destroy(1, 1);
        lifeSwiperActive = false;
      } else if($(this).innerWidth() < 768 && lifeSwiperActive === false) {
        lifeSwiper = new Swiper('.js-life-swiper', lifeSwiperOption);
        lifeSwiperActive = true;
      } 
  
    })
  }
  
  


})

