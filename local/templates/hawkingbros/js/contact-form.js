

// отправка формы ajax 
function ajaxFormSend(form, formSerializer)
{
    $.ajax({
        method: form.method,
        url: form.action,
        data: formSerializer,
        success : function (data) {
            var data = JSON.parse(data); 
            alert(data.message);
        }
    })
}

$(document).ready(function(){
    $('.contact-form').submit(
        function(e){
            e.preventDefault(); 
            var selector = '.contact-form'; 
            var formContact = document.querySelector(selector);
            ajaxFormSend(formContact, $(this).serialize()); 
            return false;
        }
    ); 
})
