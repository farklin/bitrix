<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;


class Users
{

    public function __construct($id_hlblock)
    {
        $hlbl = $id_hlblock; // идентификатор HL блока.
        $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $this->entity_data_class = $entity->getDataClass();
    }

    public function add($login, $email)
    {
        $data = [
            'UF_LOGIN' => $login,
            'UF_EMAIL' => $email,
        ];
        $this->entity_data_class::add($data);
    }

    public function get()
    {
        $rsData = $this->entity_data_class::getList([
            'select' => ['*'], //список запрашиваемых полей, * - все поля
            'order' => [
                'ID' => 'ASC' //сортировка, в данном случае по идентификатору
            ],
            'filter' => []
        ]);
        //получение значений выборки
        while ($arData = $rsData->Fetch()) {
            var_dump($arData); //вывод одной записи
        }
    }

    public function update($id, $login, $email)
    {
        $data = [
            'UF_LOGIN' => $login,
            'UF_EMAIL' => $email,
        ];
        $this->entity_data_class::update($id, $data);
    }

    public function delete($id)
    {
        $this->entity_data_class::delete($id);
    }
}
